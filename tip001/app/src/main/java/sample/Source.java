package sample;

import lombok.Data;

@Data
public class Source {
    private int id;
    private String name;
}
