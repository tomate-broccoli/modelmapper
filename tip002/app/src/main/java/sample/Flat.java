package sample;

import lombok.Data;

@Data
public class Flat {
    private int headerId;
    private String headerName;
    private int detailId;
    // private String detailName;
    // private String detailValue;
}
