package sample;

import lombok.Data;

@Data
public class Header {
    private int id;
    private String name;
}

