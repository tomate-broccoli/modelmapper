package sample;

import lombok.Data;

@Data
public class Detail {
    private int id;
    private String name;
    private String value;
}

